#!/bin/bash
# vim:noet

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


error() {
	echo "$1" >&2
	exit 1
}

configfile="$(dirname "$0")/runit-service-creation.config"
source "$configfile"
[ -n "$ACTIVE_SERVICE_DIR" -a -n "$SUPERVISE_DIR_SYMLINK" -a -n "$SUPERVISE_DIR_SYMLINK_DEST" ] || error "Not all configuration options found in config file $configfile"

[ "$#" == 1 ] || error "This script requires exactly one argument: the name of the service to create."
[ "$1" = '-h' -o "$1" = '--help' ] && { printf "Create a new Runit service in the current directory.\nRequires exactly one argument: the name of the service to create.\n"; exit; }
[ -w "$PWD" ] || error "No permission to write in current directory $([ "$UID" -eq 0 ] || echo "(try using sudo)")"
[[ "$1" =~ ^[a-z0-9-]*$ ]] || error "Please use only lowercase letters, digits and hyphens for the service name. [a-z0-9-]"
[ -e "$1" ] && error "File or directory \`$1\` already exists!"

mkdir -p "$1"
pushd "$1" >/dev/null

# Create boilerplate run and finish scripts
printf '#!/bin/sh\nset -e # Exit if a command exits with non-zero status\nexec 2>&1 # Redirect stderr to stdout\n# Put service starting code here.\n# Remember that:\n#  * this script'"'"'s process is the one that will be supervised (so use `exec'"'"'!);\n#  * the supervised process should not fork;\n#  * the command will run as root until you tell otherwise (use chpst to run as\na different user – see its docs).' > run
chmod +x run
printf '#!/bin/sh\n' > finish

if [ $SUPERVISE_DIR_SYMLINK = true ]; then
	# Link supervise directory, e.g. to /var/run directory
	ln -s "$SUPERVISE_DIR_SYMLINK_DEST$1" supervise
fi

popd >/dev/null

./create-logging-subservice.sh "$1" || echo -e "Errors while creating log subservice\n" >&2


echo "TODO"
echo '* Complete `run`'
echo '* Either remove or complete `finish`'
echo "* Issue the following command once to register the service. It will be"
echo "  started, and also started on every following boot:"
echo "  ln -s $PWD/$1 $ACTIVE_SERVICE_DIR"
