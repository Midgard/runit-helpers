#!/bin/bash
# vim:noet

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


error() {
	echo "$1" >&2
	exit 1
}

configfile="$(dirname "$0")/runit-service-creation.config"
source "$configfile"
[ -n "$LOG_ROOT_DIR" -a -n "$LOG_OWNER" -a -n "$SUPERVISE_DIR_SYMLINK" -a -n "$SUPERVISE_DIR_SYMLINK_DEST" ] || error "Not all configuration options found in config file $configfile"

[ "$#" == 1 ] || error "This script requires exactly one argument: the name of the service for which to create a logging subservice."
[ "$1" = '-h' -o "$1" = '--help' ] && { printf "Create a new Runit logging subservice in the current directory.\nRequires exactly one argument: the name of the service for which to create a logging subservice.\n"; exit; }
[ -w "$1" ] || error "No permission to write in service $1's directory, or it doesn't exist $([ "$UID" -eq 0 ] || echo "(try using sudo)")"
[ -w "$LOG_ROOT_DIR" ] || { error "No permission to write in log root directory, or it doesn't exist $([ "$UID" -eq 0 ] || echo "(try using sudo)")"; }
[ -d "$1" ] || error "Service directory \`$1\` doesn't exist!"

mkdir -p "$1/log"
pushd "$1/log" >/dev/null

# Create logging service
mkdir -p "$LOG_ROOT_DIR/$1"
chown "$LOG_OWNER" "$LOG_ROOT_DIR/$1"
chmod 750 "$LOG_ROOT_DIR/$1"
printf '#!/bin/sh\nexec chpst -ulog svlogd -tt %s\n' "$LOG_ROOT_DIR/$1" > run
chmod +x run

if [ $SUPERVISE_DIR_SYMLINK = true ]; then
	# Link supervise directory, e.g. to /var/run directory
	ln -s "$SUPERVISE_DIR_SYMLINK_DEST$1-log" supervise
fi

popd >/dev/null

