# Runit helpers

Helper scripts for [runit](http://smarden.org/runit/)'s supervision scheme
runsv.

Will also work if you're only using runsv as a supervisor, and not the full
runit init system.

Install the scripts and the config file in the folder where you create your
service directories (probably `/etc/service` or `/service`). To create a new
service with an accompanying logging service, `cd` into said directory and
execute [`create-service.sh`](create-service.sh).

This will create a new service directory stub, create a fully functioning
logging subservice and emit a message with some reminders about what to do
next. If you don't want the logging subservice, simply delete the subdirectory
`log`.
